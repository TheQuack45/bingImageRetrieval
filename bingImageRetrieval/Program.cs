﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace bingImageRetrieval
{
    class Program
    {
        public const int MAX_RESULT_NUMBER = 150;
        public const string API_KEY_VAR_NAME = "BING_API_KEY";

        static void Main(string[] args)
        {
            string apiKey = GetApiKey();

            Console.WriteLine("Search query:");
            string query = Console.ReadLine();

            Console.WriteLine("Number of images to download:");
            int imageCount = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Index to start from (leave blank to start from index 0):");
            int startIndex = 0;
            string startIndexStr = Console.ReadLine();
            if (!string.IsNullOrEmpty(startIndexStr))
                { startIndex = Int32.Parse(startIndexStr); }

            Console.WriteLine("Base directory to create subdirectories in (leave blank to use current directory):");
            string baseDir = Console.ReadLine();
            if (string.IsNullOrEmpty(baseDir))
                { baseDir = Environment.CurrentDirectory; }

            Console.WriteLine("Folder name to download images to (leave blank to match camelcased query):");
            string dirName = Console.ReadLine();
            if (string.IsNullOrEmpty(dirName))
                { dirName = RemoveSpaces(ToTitleCase(query)); }

            List<string> imageUrls = GetResults(query, apiKey, imageCount, startIndex);
            DownloadImages(query, baseDir, dirName, imageUrls);

            Console.WriteLine("");
            Console.WriteLine("Job done!");
            Console.ReadKey();
        }

        public static string GetApiKey()
        {
            return Environment.GetEnvironmentVariable(API_KEY_VAR_NAME, EnvironmentVariableTarget.User);
        }

        public static void DownloadImages(string query, string baseDir, string dir, List<string> urls)
        {
            WebClient client = new WebClient();
            client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.33 Safari/537.36");

            System.IO.Directory.CreateDirectory(Path.Combine(baseDir, dir));

            for (int i = 0; i < urls.Count; i++)
            {
                if (i % 10 == 0)
                    { Console.WriteLine("Image {0} downloaded.", i); }

                string extension = GetExtension(urls[i]);
                try
                {
                    client.DownloadFile(urls[i], Path.Combine(baseDir, dir, (i + extension)));
                }
                catch (WebException)
                {
                    continue;
                }
            }

            client.Dispose();
            client = null;
        }

        private static string ToTitleCase(string str)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            return ti.ToTitleCase(str);
        }

        public static string GetExtension(string url)
        {
            string extensionPattern = @".[\w]{1,}$";
            return Regex.Match(url, extensionPattern).Value;
        }

        public static string RemoveSpaces(string input)
        {
            return Regex.Replace(input, @"\s+", "");
        }

        public static List<string> GetResults(string query, string apiKey, int count, int startIndex = 0)
        {
            var images = new List<string>();
            var searcher = new ImageSearchRetriever(apiKey);

            int offset = startIndex;
            int remaining = count;

            while (remaining > 0)
            {
                SearchResult search = searcher.ImageSearch(query, Math.Min(remaining, MAX_RESULT_NUMBER), offset);
                RootObject result = JsonConvert.DeserializeObject<RootObject>(search.JsonResult);
                List<Value> results = result.value;
                int resultCount = results.Count;

                remaining -= resultCount;
                offset += resultCount;

                images.AddRange(results.Select(image => image.contentUrl));
            }

            return images;
        }
    }
}
