﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace bingImageRetrieval
{
    public struct SearchResult
    {
        public string JsonResult;
        public Dictionary<string, string> RelevantHeaders;
    }

    public class ImageSearchRetriever
    {
        private const string URI_BASE = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";

        private readonly string _apiKey;

        public ImageSearchRetriever(string apiKey)
        {
            this._apiKey = apiKey;
        }

        public SearchResult ImageSearch(string query, int count, int offset = 0)
        {
            string uriQuery = URI_BASE + "?q=" + Uri.EscapeDataString(query) + "&count=" + count;
            if (offset != 0)
                { uriQuery += "&offset=" + offset; }

            WebRequest request = HttpWebRequest.Create(uriQuery);
            request.Headers["Ocp-Apim-Subscription-Key"] = this._apiKey;

            var response = (HttpWebResponse)request.GetResponseAsync().Result;
            string json = new StreamReader(response.GetResponseStream()).ReadToEnd();

            var searchResult = new SearchResult()
            {
                JsonResult = json,
                RelevantHeaders = new Dictionary<string, string>(),
            };

            foreach (string header in response.Headers)
            {
                if (header.StartsWith("BingAPIs-") || header.StartsWith("X-MSEdge-"))
                    { searchResult.RelevantHeaders[header] = response.Headers[header]; }
            }

            return searchResult;
        }
    }
}
